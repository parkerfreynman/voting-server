# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

## Pull Request

1. Fork the repository
2. Create a feature branch
3. Make changes
4. Merge everything to develop
5. Commit and make the pull request

## Code of Conduct

Please read our [code of conduct](CODE_OF_CONDUCT.md) when contributing to this repository.
