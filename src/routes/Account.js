const express = require('express')
const AccountRouter = express.Router()
const { AccountPolicy } = require('../policies')
const { AccountController } = require('../controllers')

AccountRouter.post(
  '/register',
  AccountPolicy.register,
  AccountController.register
)
AccountRouter.post(
  '/login',
  AccountPolicy.login,
  AccountController.login
)
AccountRouter.post(
  '/retrieve',
  AccountPolicy.retrieve,
  AccountController.retrieve
)
AccountRouter.post(
  '/update',
  AccountPolicy.update,
  AccountController.update
)
AccountRouter.post(
  '/logout',
  AccountPolicy.logout,
  AccountController.logout
)

module.exports = AccountRouter
