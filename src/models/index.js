const Account = require('./Account')

module.exports = {
  /**
   * The Account schema.
   * @type {Schema}
   */
  Account
}
