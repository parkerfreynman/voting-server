const { header, body } = require('express-validator/check')
const ErrorBasket = require('../middlewares/ErrorBasket')

module.exports = {
  /**
   * register policy.
   * @type {ArrayLike}
   */
  register: [
    header('token')
      .not()
      .exists(),
    body('studentNumber')
      .exists({ checkNull: true, checkFalsy: true })
      .isNumeric(),
    body('password')
      .exists({ checkNull: true, checkFalsy: true })
      .isAscii()
      .isLength({ min: 0, max: 128 }),
    body('address')
      .not()
      .exists({ checkNull: true, checkFalsy: true }),
    body('givenName')
      .optional()
      .isString()
      .isLength({ min: 0, max: 128 }),
    body('middleName')
      .optional()
      .isString()
      .isLength({ min: 0, max: 128 }),
    body('familyName')
      .optional()
      .isString()
      .isLength({ min: 0, max: 128 }),
    body('gender')
      .optional()
      .isString()
      .matches(/^(Male)|(Female)$/),
    ErrorBasket
  ],

  /**
   * login policy.
   * @type {ArrayLike}
   */
  login: [
    header('token')
      .not()
      .exists(),
    body('studentNumber')
      .exists({ checkNull: true, checkFalsy: true })
      .isNumeric(),
    body('password')
      .exists({ checkNull: true, checkFalsy: true })
      .isAscii()
      .isLength({ min: 0, max: 128 }),
    ErrorBasket
  ],

  /**
   * logout policy.
   * @type {ArrayLike}
   */
  logout: [
    header('token')
      .exists({ checkNull: true, checkFalsy: true })
      .isJWT(),
    ErrorBasket
  ],

  /**
   * retrieve policy.
   * @type {ArrayLike}
   */
  retrieve: [
    header('token')
      .exists({ checkNull: true, checkFalsy: true })
      .isJWT(),
    ErrorBasket
  ],

  /**
   * update policy.
   * @type {ArrayLike}
   */
  update: [
    header('token')
      .exists({ checkNull: true, checkFalsy: true })
      .isJWT(),
    body('studentNumber')
      .optional()
      .isNumeric(),
    body('password')
      .optional()
      .isAscii()
      .isLength({ min: 0, max: 128 }),
    body('address')
      .optional()
      .isHexadecimal(),
    body('givenName')
      .optional()
      .isString()
      .isLength({ min: 0, max: 128 }),
    body('middleName')
      .optional()
      .isString()
      .isLength({ min: 0, max: 128 }),
    body('familyName')
      .optional()
      .isString()
      .isLength({ min: 0, max: 128 }),
    body('gender')
      .optional()
      .isString()
      .matches(/^(Male)|(Female)$/),
    ErrorBasket
  ]
}
