const {
  log,
  error
} = require('./log')
const db = require('./db')
const config = require('./config')
const app = require('./app')
const fs = require('fs')
const http = require('http')
const https = require('https')

db.connect().then(function () {
  if (config.https) {
    https
      .createServer(
        {
          key: fs.readFileSync(config.key),
          cert: fs.readFileSync(config.cert)
        },
        app
      )
      .listen(config.port, config.host, function () {
        log(`[HTTPS] Listening to ${config.host}:${config.port}`)
      })
  } else {
    http
      .createServer(app)
      .listen(config.port, config.host, function () {
        log(`[HTTP] Listening to ${config.host}:${config.port}`)
      })
  }
}, err => {
  error('Unable to connect to database.' + err)
})
