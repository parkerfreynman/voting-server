const express = require('express')
const app = express()
const helmet = require('helmet')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(helmet())
app.use(cors())
app.use(morgan('combined'))
app.use(bodyParser.json())
app.disable('x-powered-by')
require('./routes')(app)

module.exports = app
