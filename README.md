# voting-server

[![pipeline status](https://gitlab.com/parkerfreynman/voting-server/badges/master/pipeline.svg)](https://gitlab.com/parkerfreynman/voting-server/commits/master)
[![coverage report](https://gitlab.com/parkerfreynman/voting-server/badges/master/coverage.svg)](https://gitlab.com/parkerfreynman/voting-server/commits/master)

A very basic centralized voting backend.

## Getting Started

### Prerequisites

For local development:

- nodejs
- yarn
- mongodb

For containerized environment:

- docker
- nodejs image
- mongodb image

### Installation

To clone the repository, simply type in the terminal:

```bash
git clone https://gitlab.com/parkerfreynman/voting-server
```

Then install the dependencies using:

```bash
yarn
```

or

```bash
npm install
```

## Testing and Linting

ESlint is used in this repository. It follows the standard rules. To check if the code complies with it, type in:

```bash
yarn run lint
```

To run unit tests, do:

```bash
yarn run test
```

Or run the tests with code coverage:

```bash
yarn run coverage
```

## Deployment

This project is meant to be deployed as a containerized application. To build the image, issue the command:

```bash
docker build .
```

You can deploy it using docker, docker swarm, or in kubernetes.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

## Authors

[Jerico Manapsal](https://gitlab.com/jericomanapsal)

## License

[MIT](LICENSE)

## Acknowledgements

Thanks to [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2) for providing a publicly-available README templates.
