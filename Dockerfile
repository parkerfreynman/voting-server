FROM node

LABEL maintainer="Jerico Manapsal"
LABEL version="0.0.1"
LABEL description="A very basic centralized voting backend."

ENV NODE_ENV production
EXPOSE 8080

ADD package.json /tmp/package.json
RUN cd /tmp && yarn
RUN cp -a /tmp/node_modules /usr/src/app/

WORKDIR /usr/src/app
ADD . /usr/src/app

CMD ["yarn", "run", "start"]
