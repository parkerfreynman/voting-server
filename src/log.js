const fancyLog = require('fancy-log')
const chalk = require('chalk')

/**
 * Logs the data.
 * @param {*} data Data to be logged.
 */
function log (data) {
  fancyLog(data)
}

/**
 * Logs the data as error.
 * @param {*} data Data to be logged.
 */
function error (data) {
  fancyLog.error(
    chalk.white.bgRed(' ERROR ') + ' ' + data
  )
}

/**
 * Logs the data as warning.
 * @param {*} data Data to be logged.
 */
function warn (data) {
  fancyLog.warn(
    chalk.black.bgYellow(' WARN  ') + ' ' + data
  )
}

/**
 * Logs the data as information.
 * @param {*} data Data to be logged.
 */
function info (data) {
  fancyLog.info(
    chalk.white.bgBlue(' INFO  ') + ' ' + data
  )
}

/**
 * Logs the data and expands it.
 * @param {*} data Data to be logged.
 */
function dir (data) {
  fancyLog.dir(data)
}

module.exports = {
  log,
  error,
  warn,
  info,
  dir
}
