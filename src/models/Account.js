const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AccountSchema = new Schema({
  studentNumber: {
    type: Number,
    unique: true,
    required: true,
    trim: true,
    index: true
  },
  password: {
    type: String,
    required: true
  },
  address: {
    type: String,
    unique: true,
    required: true,
    trim: true,
    index: true
  },
  givenName: String,
  middleName: String,
  familyName: String,
  gender: String
}, { timestamps: true })

module.exports = mongoose.model('Account', AccountSchema)
