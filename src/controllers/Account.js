const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt-nodejs')
const wallet = require('ethereumjs-wallet')
const { Account } = require('../models')
const config = require('../config')
const { getUser } = require('../util')

module.exports = {
  /**
   * Creates a new account.
   * @param {Object} req Request object from express.
   * @param {Object} res Response object from express.
   */
  async register(req, res) {
    bcrypt.hash(req.body.password, null, null, (_, hash) => {
      Account.create({
        ...req.body,
        password: hash,
        address: wallet.generate().getAddressString()
      }).then(() => {
        res.status(200).send({})
      })
    })
  },

  /**
   * Creates a new session for the user.
   * @param {Object} req Request object from express.
   * @param {Object} res Response object from express.
   */
  async login(req, res) {
    Account.findOne({
      studentNumber: req.body.studentNumber
    }).then(acc => {
      if (!acc) {
        res.status(500).send({ msg: 'Account does not exists.' })
        return
      }
      bcrypt.compare(
        req.body.password,
        acc.password,
        (_, result) => {
          if (!result) {
            res.status(500).send({ msg: 'Invalid password.' })
            return
          }
          res.status(200).send({
            token: jwt.sign(
              { studentNumber: acc.studentNumber },
              config.serverKey,
              { expiresIn: 86400 }
            )
          })
        }
      )
    })
  },

  /**
   * Retrieves the data of current user.
   * @param {Object} req Request object from express.
   * @param {Object} res Response object from express.
   */
  async retrieve(req, res) {
    let user = await getUser(req)
    if (!user) {
      res.status(500).send({ msg: 'Account does not exists.' })
      return
    }
    delete user.password
    res.status(200).send(user)
  },

  /**
   * Modifies the fields of the current user.
   * @param {Object} req Request object from express.
   * @param {Object} res Response object from express.
   */
  async update(req, res) {
    const user = await getUser(req)
    if (!user) {
      res.status(500).send({ msg: 'Account does not exists.' })
      return
    }
    Account.findOneAndUpdate(
      {
        studentNumber: user.studentNumber
      },
      {
        ...user,
        ...req.body
      }
    ).then(() => {
      res.status(200).send({})
    })
  },

  /**
   * Destroys the current session of the user.
   * @param {Object} req Request object from express.
   * @param {Object} res Response object from express.
   */
  async logout(req, res) {
    res.status(200).send()
  }
}
