const expect = require('chai').expect
const request = require('supertest')
const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const db = require('../src/db')
const config = require('../src/config')
const routes = require('../src/routes')
const { Account } = require('../src/models')

let app = express()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
routes(app)

describe('Account', function () {
  this.timeout(3000)
  let agent = request.agent(app)
  before(function (done) {
    db.connect().then(done)
  })
  before(function (done) {
    Account.findOneAndDelete({
      studentNumber: 1
    }).then(() => {
      done()
    }, done)
  })
  before(function (done) {
    agent
      .post('/register')
      .set('Accept', 'application/json')
      .send({
        studentNumber: 1,
        password: 'helloworld'
      })
      .end(function (err, res) {
        if (err) throw err
        else done()
      })
  })
  before(function (done) {
    Account.findOneAndDelete({
      studentNumber: 1
    }).then(() => {
      done()
    }, done)
  })
  after(function (done) {
    db.disconnect().then(done)
  })
  describe('register', function () {
    const userData = {
      studentNumber: 123,
      password: 'helloworld'
    }
    before(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    after(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    it('should create a new account', function (done) {
      agent
        .post('/register')
        .set('Accept', 'application/json')
        .send(userData)
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(200)
          done()
        })
    })
  })
  describe('login', function () {
    const userData = {
      studentNumber: 124,
      password: 'helloworld'
    }
    before(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    before(function (done) {
      agent
        .post('/register')
        .set('Accept', 'application/json')
        .send(userData)
        .end(function (err, res) {
          if (err) throw err
          else done()
        })
    })
    after(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    it('should login the created account', function (done) {
      agent
        .post('/login')
        .set('Accept', 'application/json')
        .send(userData)
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(200)
          expect(res.body).to.be.a('object')
          expect(res.body).to.have.property('token')
          expect(res.body.token).to.be.a('string')
          done()
        })
    })
    it('should not login the non-existent account', function (done) {
      agent
        .post('/login')
        .set('Accept', 'application/json')
        .send({
          studentNumber: 1234,
          password: 'something'
        })
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(500)
          expect(res.body).to.be.a('object')
          expect(res.body).to.have.property('msg')
          expect(res.body.msg).to.be.a('string')
          done()
        })
    })
    it('should not login due to invalid password', function (done) {
      agent
        .post('/login')
        .set('Accept', 'application/json')
        .send({
          ...userData,
          password: 'wrong_password'
        })
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(500)
          expect(res.body).to.be.a('object')
          expect(res.body).to.have.property('msg')
          expect(res.body.msg).to.be.a('string')
          done()
        })
    })
  })
  describe('retrieve', function () {
    const userData = {
      studentNumber: 125,
      password: 'helloworld'
    }
    let token
    before(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    before(function (done) {
      agent
        .post('/register')
        .set('Accept', 'application/json')
        .send(userData)
        .end(function (err, res) {
          if (err) throw err
          else done()
        })
    })
    before(function (done) {
      agent
        .post('/login')
        .set('Accept', 'application/json')
        .send(userData)
        .end(function (err, res) {
          if (err) throw err
          else {
            token = res.body.token
            done()
          }
        })
    })
    after(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    it('should retrieve the details of the account', function (done) {
      agent
        .post('/retrieve')
        .set('Accept', 'application/json')
        .set('token', token)
        .send(userData)
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(200)
          expect(res.body).to.be.a('object')
          done()
        })
    })
    it('should not retrieve non-existent account', function (done) {
      agent
        .post('/retrieve')
        .set('Accept', 'application/json')
        .set(
          'token',
          jwt.sign(
            { studentNumber: 521 },
            'wrong_signing_key',
            { expiresIn: 86400 }
          )
        )
        .send(userData)
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(500)
          expect(res.body).to.be.a('object')
          done()
        })
    })
    it('should not retrieve due to invalid session', function (done) {
      agent
        .post('/retrieve')
        .set('Accept', 'application/json')
        .set(
          'token',
          jwt.sign(
            { hello: 'world' },
            config.serverKey,
            { expiresIn: 86400 }
          )
        )
        .send(userData)
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(500)
          expect(res.body).to.be.a('object')
          done()
        })
    })
  })
  describe('update', function () {
    const userData = {
      studentNumber: 126,
      password: 'helloworld'
    }
    let token
    before(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    before(function (done) {
      agent
        .post('/register')
        .set('Accept', 'application/json')
        .send(userData)
        .end(function (err, res) {
          if (err) throw err
          else done()
        })
    })
    before(function (done) {
      agent
        .post('/login')
        .set('Accept', 'application/json')
        .send(userData)
        .end(function (err, res) {
          if (err) throw err
          else {
            token = res.body.token
            done()
          }
        })
    })
    after(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    it('should update the account', function (done) {
      agent
        .post('/update')
        .set('Accept', 'application/json')
        .set('token', token)
        .send({
          givenName: 'hello'
        })
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(200)
          done()
        })
    })
    it('should not update because user is not logged in', function (done) {
      agent
        .post('/update')
        .set('Accept', 'application/json')
        .send({
          givenName: 'hello'
        })
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(500)
          expect(res.body).to.be.a('object')
          expect(res.body.msg).to.be.a('string')
          done()
        })
    })
    it('should not update due to invalid session', function (done) {
      agent
        .post('/update')
        .set('Accept', 'application/json')
        .set(
          'token',
          jwt.sign(
            { studentNumber: 621 },
            'wrong_signing_key',
            { expiresIn: 86400 }
          )
        )
        .send({
          givenName: 'hello'
        })
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(500)
          expect(res.body).to.be.a('object')
          expect(res.body.msg).to.be.a('string')
          done()
        })
    })
  })
  describe('logout', function () {
    const userData = {
      studentNumber: 127,
      password: 'helloworld'
    }
    let token
    before(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    before(function (done) {
      agent
        .post('/register')
        .set('Accept', 'application/json')
        .send(userData)
        .end(function (err, res) {
          if (err) throw err
          else done()
        })
    })
    before(function (done) {
      agent
        .post('/login')
        .set('Accept', 'application/json')
        .send(userData)
        .end(function (err, res) {
          if (err) throw err
          else {
            token = res.body.token
            done()
          }
        })
    })
    after(function (done) {
      Account.findOneAndDelete({
        studentNumber: userData.studentNumber
      }).then(() => {
        done()
      }, done)
    })
    it('should update the account', function (done) {
      agent
        .post('/logout')
        .set('Accept', 'application/json')
        .set('token', token)
        .send({})
        .end(function (err, res) {
          expect(err).to.be.null
          expect(res.status).to.be.eql(200)
          done()
        })
    })
  })
})
