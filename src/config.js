require('dotenv').config()

module.exports = {
  /**
   * Host where the server will bind to.
   * @type {String}
   */
  host: process.env.HOST,

  /**
   * Port number where the server will bind to.
   * @type {Number}
   */
  port: parseInt(process.env.PORT),

  /**
   * MongoDB URI to be used as primary database server.
   * @type {String}
   */
  db: process.env.DB,

  /**
   * Key to be used when generating JSON web tokens.
   * @type {String}
   */
  serverKey: process.env.SERVER_KEY,

  /**
   * Indicates whether https should be used.
   * @type {Boolean}
   */
  https: parseInt(process.env.HTTPS) === 1,

  /**
   * The key file for HTTPS.
   * @type {String}
   */
  key: process.env.KEY,

  /**
   * The cert file for HTTPS.
   * @type {String}
   */
  cert: process.env.CERT,

  /**
   * Minio bucket name.
   * @type {String}
   */
  bucketName: process.env.BUCKET,

  /**
   * Contains options for Minio client.
   * @type {Object}
   */
  minio: {
    /**
     * The endpoint where Minio server is running.
     * @type {String}
     */
    endPoint: process.env.MINIO_ENDPOINT,

    /**
     * The port at which Minio server is running.
     * @type {Number}
     */
    port: parseInt(process.env.MINIO_PORT),

    /**
     * Determines if the connection should be secure.
     * @type {Boolean}
     */
    useSSL: parseInt(process.env.MINIO_SECURE) !== 0,

    /**
     * The access key used to communicate with Minio server.
     * @type {String}
     */
    accessKey: process.env.MINIO_ACCESS_KEY,

    /**
     * The secret key used to communicate with Minio server.
     * @type {String}
     */
    secretKey: process.env.MINIO_SECRET_KEY
  }
}
