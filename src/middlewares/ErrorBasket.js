const { validationResult } = require('express-validator/check')

/**
 * Catches all error in the middleware pipeline and sets an appropriate response.
 * @param {Object} req Request object from express.
 * @param {Object} res Response object from express.
 * @param {Object} next Middleware traversal object from express.
 */
module.exports = function (req, res, next) {
  const err = validationResult(req)
  if (err.isEmpty()) next()
  else {
    res.status(500).send({
      msg: 'Validation error.',
      data: err.array()
    })
  }
}
