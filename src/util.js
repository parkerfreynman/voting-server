const jwt = require('jsonwebtoken')
const config = require('./config')
const { Account } = require('./models')

/**
 * Extracts the student number from the token in header.
 * @param {Object} req Request object from express.
 * @returns {PromiseLike<Number>} The student number of the user.
 */
async function getStudentNumber(req) {
  return new Promise(async resolve => {
    const token = req.get('token')
    jwt.verify(token, config.serverKey, (err, decoded) => {
      if (err) resolve(null)
      else {
        if (decoded.studentNumber) {
          resolve(decoded.studentNumber)
        } else resolve(null)
      }
    })
  })
}

/**
 * Extracts the student number from the token in header.
 * Then queries the database to retrieve user information.
 * @param {Object} req Request object from express.
 * @returns {PromiseLike<Object>} The details of the user.
 */
async function getUser(req) {
  return new Promise(async resolve => {
    const studentNumber = await getStudentNumber(req)
    if (studentNumber) {
      Account.findOne({ studentNumber }).then(acc => {
        resolve(acc.toObject())
      })
    } else resolve(null)
  })
}

module.exports = {
  getStudentNumber,
  getUser
}
