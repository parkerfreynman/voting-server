const { error } = require('./log')
const mongoose = require('mongoose')
// const MinioClient = require('minio').Client
const config = require('./config')
// let minio
let connection

module.exports = {
  /**
   * Connects to the database.
   * @returns {PromiseLike}
   */
  connect: function () {
    return new Promise(function (resolve, reject) {
      mongoose.connect(
        config.db,
        {
          useNewUrlParser: true,
          useCreateIndex: true,
          useFindAndModify: false
        }
      ).then(function () {
        mongoose.Promise = global.Promise
        connection = mongoose.connection
        connection.on('error', error)
        // minio = new MinioClient(config.minio)
        // minio.bucketExists(config.bucketName).then(async exists => {
        //   if (!exists) await minio.makeBucket(config.bucketName, 'local')
        // })
        resolve()
      }, reject)
    })
  },

  /**
   * Disconnects from the database.
   * @returns {PromiseLike}
   */
  disconnect: async function () {
    return new Promise(resolve => {
      connection.close()
      resolve()
    })
  }
  // /**
  //  * Retrieves the Minio client.
  //  * @returns {MinioClient} The Minio client.
  //  */
  // minio: function () { return minio }
}
