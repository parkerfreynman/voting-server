const AccountRouter = require('./Account')

module.exports = app => {
  app.use(AccountRouter)
}
